<?php

namespace App;
use Ixudra\Curl\Facades\Curl;

class Finder
{
    private $endPoint = 'https://rmp.files.bbci.co.uk/technical-test/source-data.json';

    /**
     * Getting the source data.
     * 
     * @param  $q
     *
     * @return array
     */
    public function getProgramme($q = '')
    {
        $response = Curl::to($this->endPoint)
            ->get();
        $programme = json_decode($response, true);
        $programmeList = $programme['atoz']['tleo_titles'];
        $programmeListItems = [];
        if (!empty($q)) {
            foreach ($programmeList as $item) {
                if (strpos($item['slice_title'], $q) !== false) {
                    $programmeListItems[] = $item;
                }
            }
            return $programmeListItems;
        }
        return $programmeList;
    }
}