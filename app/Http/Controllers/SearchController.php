<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Finder;

class SearchController extends Controller
{
    /**
     * Display a list of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Finder $finder, Request $request)
    {
        $q = $request->input('search');
        $programmeList = $finder->getProgramme($q);
        return view('app', ['list' => $programmeList]);
    }
}
