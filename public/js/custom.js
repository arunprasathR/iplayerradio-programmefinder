$(function(){

	//Showing the results when typing a string on the input search field
	$("input").on('keyup', function(){

		var length = $(".list-group-item").length;

		var val = $.trim($(this).val()).replace(/\s/g, '').toUpperCase();
		console.log(val);

		if (val.length > 0) {

			for (i = 0; i < length; i++) {

		        var div = $(".col-md-6").eq(i).find('h3').attr('id').toUpperCase();

		        if (div.indexOf(val) != -1) {
		            $(".list-group-item").eq(i).show();
		            $('strong').remove();
		        } else {
		            $(".list-group-item").eq(i).hide();
		        }
	    	}
		}else {
			$(".list-group-item").show();
			$('strong').remove();
		}

		var hiddenLength = $('.list-group-item:hidden').length;

		//Displaying the error message when the search string does not match with search results
	    if(hiddenLength == length) {
				$(".list-group-item").hide();
				if ($('strong').length == 0) {
				$('input').after( "<strong>There are no results</strong>" );
			}
		}
	});

	//Showing "Loading" feature when typing a serach string in the search box could not be completed
	// I would have done this feature using jquery animation.

});