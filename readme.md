## Programme Finder
### Description
    Displaying the list of iplayer programmmes and showing the results based on the user search.

### Installation
    Laravel 5.5 installation process "https://laravel.com/docs/5.5/installation
    Laravel Mix for bootstrap and jquery  "https://laravel.com/docs/5.5/mix"

    
##### Finder
    The finder class gets the source data and returns the result.
    
##### ProgrammeTest
    The programmetest class runs a series of test cases to find whether finder class returns the expected search result.