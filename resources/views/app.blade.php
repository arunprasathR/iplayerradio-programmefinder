<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>IPlayerRadio</title>
    
    <!-- Styles -->
    {{ Html::style('css/app.css')}}
    
    <!-- Custom Styles -->
    {{ Html::style('css/custom.css')}}
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">

                <!-- Search Bar -->
                <div class="page-header ">
                    <input type="text" class="form-control"  placeholder="Begin typing to search for a programme">
                    @if(empty($list))
                        <strong> There are no results </strong>
                    @endif
                </div>
                
                <!-- Search List -->
                <div class="list-group">
                @foreach($list as $item)
                        <a class="list-group-item clearfix">
                            <div class="col-md-4 col-md-offset-1">
                                @if(!empty($item['programme']['image']))
                                    <img src="https://ichef.bbci.co.uk/images/ic/480x270/{{ $item['programme']['image']['pid'] }}.jpg" alt="image" width="100%" height="200" >
                                    @else
                                    <img src="https://ichef.bbci.co.uk/images/ic/480x270/p02wkrw1.jpg" alt="image" width="100%" height="200" >
                                @endif
                            </div>
                            <div class="col-md-6">
                                <h3 id="{{ $item['slice_title'] }}">{{ $item['programme']['title'] }}</h3>
                                <p>{{ $item['programme']['short_synopsis'] }}</p>
                            </div>
                        </a>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

    <!-- JavaScripts -->
    <script src="js/app.js"></script>
    <script src="js/custom.js"></script>
</script>
</body>
</html>