<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Finder;

class ProgrammeTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testHasProgrammes()
    {
        $this->assertTrue($this->checkProgramme('title', 'A Bombay Symphony'));
        $this->assertFalse($this->checkProgramme('title', 'Arun'));
        $this->assertTrue($this->checkProgramme('title', 'A Brief History of Mathematics', 'history'));
    }

    public function checkProgramme($key, $value, $q = '')
    {
        $finder = new Finder();
        $list = $finder->getProgramme($q);
        foreach ($list as $item) {
            if ($item[$key] == $value) {
                return true;
            }
        }
        return false;
    }
}
